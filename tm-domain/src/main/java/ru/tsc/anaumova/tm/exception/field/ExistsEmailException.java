package ru.tsc.anaumova.tm.exception.field;

import ru.tsc.anaumova.tm.exception.AbstractException;

public final class ExistsEmailException extends AbstractException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}