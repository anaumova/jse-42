package ru.tsc.anaumova.tm.exception.system;

import ru.tsc.anaumova.tm.exception.AbstractException;

public final class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error! Status is empty...");
    }

}