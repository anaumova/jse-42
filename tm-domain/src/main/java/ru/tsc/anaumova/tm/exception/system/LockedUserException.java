package ru.tsc.anaumova.tm.exception.system;

import ru.tsc.anaumova.tm.exception.AbstractException;

public class LockedUserException extends AbstractException {

    public LockedUserException() {
        super("Error! User is locked...");
    }

}