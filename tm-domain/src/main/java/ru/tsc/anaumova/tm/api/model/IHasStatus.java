package ru.tsc.anaumova.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}